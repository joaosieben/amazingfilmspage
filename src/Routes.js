import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Films from './pages/films/Films';
import FilmDetail from './pages/film-detail/FilmDetail';

export const Routes = () => (
  <Switch>
    <Route exact path="/">
      <Redirect to="/films" />
    </Route>
    <Route path="/films" component={Films} />
    <Route path="/film/:key" component={FilmDetail} />
    <Route component={() => <h1>Not Found!</h1>} />
  </Switch>
);

export default Routes;
