import React from 'react';
import { Link } from 'react-router-dom';
import MovieFilterIcon from '@material-ui/icons/MovieFilter';

export const Header = () => (
  <header id="header">
    <Link to="/films" className="header-link">
      <MovieFilterIcon className="header-logo" />
      <h1 className="header-title">Amazing Films Page</h1>
    </Link>
  </header>
);

export default Header;
