import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Header from '../Header';

describe('Header component', () => {
  it('Renders component properly', () => {
    expect(
      <BrowserRouter>
        <Header />
      </BrowserRouter>,
    ).toMatchSnapshot();
  });
});
