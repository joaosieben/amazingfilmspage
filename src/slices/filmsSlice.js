import { createSlice } from '@reduxjs/toolkit';
import filmsList from '../content/movie.mock-data.json';

export const filmsSlice = createSlice({
  name: 'films',
  initialState: {},
  reducers: {
    fetchAll: (state, action) => {
      state.films = action.payload;
    },
    fetchOne: (state, action) => {
      state.film = filmsList.find(({ key }) => key === action.payload);
    },
  },
});

export const { fetchAll, fetchOne } = filmsSlice.actions;

export const selectFilms = (state) => state.films;

export const filmsFetch = () => (dispatch) => dispatch(fetchAll(filmsList));

export default filmsSlice.reducer;
