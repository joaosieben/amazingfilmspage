/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Container from '@material-ui/core/Container';
import { Link } from 'react-router-dom';
import WatchLaterIcon from '@material-ui/icons/WatchLater';
import StarsIcon from '@material-ui/icons/Stars';
import TheatersIcon from '@material-ui/icons/Theaters';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import S from 'string';
import { fetchOne } from '../../slices/filmsSlice';

class FilmDetail extends React.Component {
  constructor(props) {
    super(props);
    const { match: { params: { key } } } = this.props;
    // eslint-disable-next-line react/destructuring-assignment
    this.props.fetchOne(key);
  }

  render() {
    const { film } = this.props;

    if (film) {
      return (
        <Container maxWidth="md" id="film-detail">
          <div className="film-data">
            <section className="left">
              <img src={`${process.env.PUBLIC_URL}/movie-covers/${film.img}`} alt={`${film.name} Film cover`} className="film-cover" />
            </section>
            <section className="right">
              <h2 className="film-title">{film.name}</h2>
              <p className="film-length">
                <WatchLaterIcon className="icon" />
                {' '}
                {film.length}
              </p>
              <p className="film-rating">
                <StarsIcon className="icon" />
                {' '}
                {film.rate}
              </p>
              <p className="film-genres">
                <TheatersIcon className="icon" />
                {film.genres && film.genres.map((genre, index) => (index === film.genres.length - 1
                  ? S(genre).humanize().s
                  : `${S(genre).humanize().s}, `))}
              </p>
              <p className="film-description">{film.description}</p>
            </section>
          </div>
          <Link to="/films" className="link-back">
            <ArrowBackIosIcon className="icon-back" />
            Back to main page
          </Link>
        </Container>
      );
    }
    return (<p>Film not found!</p>);
  }
}

const mapStateToProps = ({ films: { film } }) => ({ film });

const mapDispatchToProps = { fetchOne };

FilmDetail.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      key: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  film: PropTypes.shape({
    key: PropTypes.string,
    name: PropTypes.string,
    description: PropTypes.string,
    genres: PropTypes.arrayOf(PropTypes.string),
    rate: PropTypes.string,
    length: PropTypes.string,
    img: PropTypes.string,
  }),
  fetchOne: PropTypes.func.isRequired,
};

FilmDetail.defaultProps = {
  film: {},
};

export default connect(mapStateToProps, mapDispatchToProps)(FilmDetail);
