import React from 'react';
import FilmDetail from '../FilmDetail';

describe('FilmDetail page', () => {
  it('Renders component properly', () => {
    const props = {
      match: {
        params: {
          key: 'some-key',
        },
      },
      fetchOne: jest.fn(),
    };

    expect(<FilmDetail {...props} />).toMatchSnapshot();
  });
});
