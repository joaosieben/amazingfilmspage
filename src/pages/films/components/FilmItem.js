import React from 'react';
import PropTypes from 'prop-types';

export const FilmItem = ({
  name, rate, img, length,
}) => (
  <div className="film-item">
    <section className="left">
      <img src={`${process.env.PUBLIC_URL}/movie-covers/${img}`} alt={`${name} Film cover`} />
    </section>
    <section className="right">
      <div className="text">
        <h2 className="film-title">{name}</h2>
        <p className="film-rating">
          Rating:
          {' '}
          {rate}
        </p>
        <p className="film-length">
          {length}
        </p>
      </div>
    </section>
  </div>
);

FilmItem.propTypes = {
  name: PropTypes.string.isRequired,
  rate: PropTypes.string.isRequired,
  img: PropTypes.string.isRequired,
  length: PropTypes.string.isRequired,
};

export default FilmItem;
