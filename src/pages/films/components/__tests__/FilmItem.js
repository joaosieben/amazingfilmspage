import React from 'react';
import FilmItem from '../FilmItem';

describe('FilmItem Component', () => {
  it('Renders component properly', () => {
    const props = {
      name: 'someName',
      rate: 'someRate',
      img: 'someImg',
      length: 'someLength',
    };

    expect(<FilmItem {...props} />).toMatchSnapshot();
  });
});
