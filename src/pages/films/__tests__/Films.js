import { ExpansionPanelActions } from '@material-ui/core';
import React from 'react';
import Films from '../Films';

describe('Films page', () => {
  it('Renders component properly', () => {
    const wrapper = shallow(<Films />);
    expect(wrapper).toMatchSnapshot();
  });
});
