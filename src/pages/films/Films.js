import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { selectFilms, filmsFetch } from '../../slices/filmsSlice';
import FilmItem from './components/FilmItem';

export const Films = () => {
  const { films } = useSelector(selectFilms);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(filmsFetch());
  }, []);
  const [searchTerm, setSearchTerm] = useState('');
  const filteredList = !searchTerm
    ? films
    : films.filter((film) => film.name.toLowerCase().includes(searchTerm.toLowerCase()));

  return (
    <Container maxWidth="lg" id="films">
      <Autocomplete
        className="films-search"
        options={films || []}
        getOptionLabel={(film) => film.name}
        renderInput={(params) => <TextField {...params} label="Search films" variant="outlined" value={searchTerm} />}
        onChange={(event, value) => setSearchTerm(value ? value.name : '')}
      />
      {filteredList && filteredList.map((film) => (
        <Link to={`/film/${film.key}`} key={film.key} className="film-link">
          <FilmItem {...film} key={film.key} />
        </Link>
      ))}
    </Container>
  );
};

export default Films;
