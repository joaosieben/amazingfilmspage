# Amazing Films Page | Joao Sieben
This is my Amazing Films Page. It contains a list of amazing films and information about them.

## What was used
- The official Redux+JS template for [Create React App](https://github.com/reduxjs/cra-template-redux), which supports [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) out of the box;
- [React Router](https://reactrouter.com/) to set up routes/history/etc;
- [SCSS](https://sass-lang.com/) to compile stylesheets;
  
## Available scripts
```
yarn start
```
To run the application in dev mode (or `npm run start` works as well).

```
yarn test
```
To execute unit tests.

```
yarn build
```
To generate a production build in `build` folder.

```
yarn lint
```
To view code formatting errors

```
yarn lint-fix
```
To fix errors (when possible).

That's all. Enjoy!